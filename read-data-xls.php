<?php
	require 'vendor/autoload.php';

	if($_POST) {

		$input_file = "./sample-data2.xls";

		$reader = new PhpOffice\PhpSpreadsheet\Reader\Xls();
		$spreadsheet = $reader->load($input_file);
		$worksheet = $spreadsheet->getActiveSheet();
		$max_row_index = $worksheet->getHighestRow();
		$max_column = $worksheet->getHighestColumn();
		$max_column_index = PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($max_column);

		$width = $_POST["width"];
		$drop = $_POST["drop"];
		$width_index = 1;
		$drop_index = 1;

		for($i = 1; $i < $max_column_index; $i++) {
			$current_width = $worksheet->getCellByColumnAndRow($i, 1)->getValue();
			
			if($current_width >= $width) {
				$width_index = $i;
				break;
			}
		}

		for($j = 1; $j < $max_row_index; $j++) {
			$current_drop = $worksheet->getCellByColumnAndRow(1, $j)->getValue();
			
			if($current_drop >= $drop) {
				$drop_index = $j;
				break;
			}
		}

		$final_cell_value = $worksheet->getCellByColumnAndRow($width_index, $drop_index)->getValue();

		echo "<p>Width: " . $width . "</p>\n";
		echo "<p>Width Index: " . $width_index . "</p>\n";
		echo "<p>Drop: " . $drop . "</p>\n";
		echo "<p>Drop Index: " . $drop_index . "</p>\n";
		echo "<p>Price Value: " . $final_cell_value;
	}

?>
